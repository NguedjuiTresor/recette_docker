package com.recette;

import com.recette.model.Recette;
import com.recette.repository.RecetteRepository;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DemoApplicationTests {

	@Autowired
	private RecetteRepository recetteRepository;

	@Test
	@Rollback(value = false)
	public void a_saveRecetteTest(){
		Recette pizza = new Recette();

		pizza.setName("Pizza");
		pizza.setIngredients("Farine");
		pizza.setLarecette("It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout");

		recetteRepository.save(pizza);

		assertTrue(pizza.getId() > 0);
	}

	@Test
	public void b_getRecetteTest(){

		Recette recette = recetteRepository.findById(1L).get();
		assertTrue(Objects.equals(recette.getId(), 1L));
	}

	@Test
	public void c_getListOfRecetteTest(){

		List list = recetteRepository.findAll();
		assertTrue(list.size() > 0);

	}

	@Test
	@Rollback(value = false)
	public void d_updateRecetteTest(){
		Recette recette = recetteRepository.findById(1L).get();
		recette.setName("ratatouille");
		recetteRepository.save(recette);
		assertNotEquals("pizza", recetteRepository.findById(1L).get().getName());

	}

	@Test
	@Rollback(value = false)
	public void deleteRecetteTest(){
		Recette pizza = new Recette();

		pizza.setName("Pizza");
		pizza.setIngredients("Farine");
		pizza.setLarecette("It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout");

		Recette saved = recetteRepository.save(pizza);
		Long id = saved.getId();

		recetteRepository.deleteById(id);

		Optional<Recette> find = recetteRepository.findById(id);

		assertTrue(find.isEmpty());
	}
}
