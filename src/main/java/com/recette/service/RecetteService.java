package com.recette.service;

import com.recette.exception.ResourceNotFoundException;
import com.recette.model.Recette;
import com.recette.repository.RecetteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RecetteService {
    @Autowired
    RecetteRepository recetteRepository;

    public void addRecette(Recette recette) {
        recetteRepository.save(recette);
    }

    public void updateRecette(Recette recette) {
        Optional<Recette> optRecette = recetteRepository.findById(recette.getId());

        if (optRecette.isPresent()) {
            Recette savedrecette = optRecette.get();
            savedrecette.setName(recette.getName());
            savedrecette.setLarecette(recette.getLarecette());
            savedrecette.setIngredients(recette.getIngredients());
            recetteRepository.save(recette);
        } else {
            throw new RuntimeException(String.format("Cannot Find recette by ID %s", recette.getId()));
        }
    }

    public Recette getRecette(String id) {
        Optional<Recette> optRecette = recetteRepository.findById(Long.valueOf(id));

        if (optRecette.isPresent()) {
            return optRecette.get();
        } else {
            throw new RuntimeException(String.format("Cannot Find recette by ID %s", id));
        }
    }
    public List<Recette> getAllRecette() {
        return recetteRepository.findAll();
    }

    public void deleteRecette(String id) {
        Optional<Recette> optRecette = recetteRepository.findById(Long.valueOf(id));

        if (optRecette.isPresent()) {
            Recette recette = optRecette.get();
            recetteRepository.delete(recette);
        } else {
            throw new RuntimeException(String.format("Cannot Find recette by ID %s", id));
        }
    }
}
