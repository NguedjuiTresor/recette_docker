package com.recette.model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Recette {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String ingredients;
    private String larecette;

    public Recette() {
    }

    public Recette(Long id, String name, String ingredients, String larecette) {
        this.id = id;
        this.name = name;
        this.ingredients = ingredients;
        this.larecette = larecette;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getLarecette() {
        return larecette;
    }

    public void setLarecette(String larecette) {
        this.larecette = larecette;
    }
}
